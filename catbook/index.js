
let db = firebase.database();
//Create
var form = document.getElementById("form");
var id = document.getElementById("id");
var firstName = document.getElementById("firstName");
var lastName = document.getElementById("lastName");
var about = document.getElementById("about");

form.addEventListener("submit", (e) => {
  e.preventDefault();
  if (!firstName.value || !lastName.value || !about.value) return null;
  var newCat = db.ref("cats").push();
  newCat.set({
    firstName: firstName.value,
    lastName: lastName.value,
    about: about.value
  });
  form.reset();
});

//Read
var catList = document.getElementById("catList");
db.ref("cats").on("child_added", (data) => {
  var div = document.createElement("div");
  div.id = data.key;
  div.setAttribute("class", "card mb-3 mt-3");
  div.innerHTML = catCard(data.val());
  catList.appendChild(div);
});

db.ref("cats").on("child_changed", (data) => {
  var cardChanged = document.getElementById(data.key);
  cardChanged.innerHTML = catCard(data.val());
});

db.ref("cats").on("child_removed", (data) => {
  var cardRemoved = document.getElementById(data.key);
  cardRemoved.parentNode.removeChild(cardRemoved);
});

catList.addEventListener("click", (e) => {
  var cardSelected = e.target.closest("div[id]");
  // // UPDATE REVEIW
   if (e.target.classList.contains("edit")) {
     firstName.value = cardSelected.querySelector(".firstName").innerText;
     about.value = cardSelected.querySelector(".about").innerText;
    lastName.value = cardSelected.querySelector(".lastName").innerText;
     id.value = cardSelected.id;
   }

  // DELETE REVEIW
  if (e.target.classList.contains("delete")) {
    var removeid = cardSelected.id;
    console.log(removeid)
    db.ref("cats/" + removeid).remove();
  }
});

function catCard({ firstName, lastName, about }) {
  return `
          <div class="row no-gutters">
            <div class="col-md-4">
              <img src="https://via.placeholder.com/400" class="card-img">
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title"><span class="lastName">${lastName}</span> <span class="firstName">${firstName}</span></h5>
                <p class="card-text"><span class="about">${about}</span></p>
              </div>
              <div class="card-footer bg-transparent border-top-0 pt-0 text-right">
                <button type="button" class="btn btn-sm btn-warning edit">
                  <i class="fas fa-edit mr-1"></i>Edit
                </button>
                <button data-id="" type="button" class="btn btn-sm btn-danger delete">
                  <i class="fas fa-trash-alt mr-1"></i>Delete
                </button>
              </div>
            </div>
          </div>
          `;
}
